#include <Arduino.h>
/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp-now-esp8266-nodemcu-arduino-ide/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

#include <ESP8266WiFi.h>
#include <espnow.h>
#include <secrets.h>

#include <Adafruit_NeoPixel.h>
// How many leds in your strip?
#define NUMPIXELS 7

// For led chips like WS2812, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806 define both DATA_PIN and CLOCK_PIN
// Clock pin only needed for SPI based chipsets when not using hardware SPI
#define PIN D4

#define ID1 (D1)
#define ID2 (D2)
#define ID3 (D5)
#define ID4 (D6)


unsigned long maxAnswerTime = 10000;
unsigned long StartTime = 0;
int aktBuzzerId = 0;
int nextMac = 0;



Adafruit_NeoPixel strip(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

uint32_t color[]={strip.Color(0,0,0),strip.Color(255,200,0),strip.Color(0,255,0),strip.Color(255,0,0),strip.Color(0,0,255)};

// Structure example to receive data
// Must match the sender structure
typedef struct struct_ReciveMessage {
    
    int id;
    uint8_t broadcastAddress[6];

} struct_ReciveMessage;
typedef struct struct_SendMessage {
    
    int id;
   

} struct_SendMessage;
typedef struct struct_mac {
    
    
    uint8_t broadcastAddress[6];

} struct_mac;

// Create a struct_message called myData
struct_ReciveMessage myData;
struct_SendMessage myDataSend;
struct_mac mac1[10];




     // Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  Serial.print("Last Packet Send Status: ");
  if (sendStatus == 0){
    Serial.println("Delivery success");
  }
  else{
    Serial.println("Delivery fail");
  }
}

// Callback function that will be executed when data is received
void OnDataRecv(uint8_t * mac, uint8_t *incomingData, uint8_t len) {

  memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("Int: ");
  Serial.println(myData.id);
  if (aktBuzzerId==0  ){
    aktBuzzerId=myData.id;
    myDataSend.id=myData.id;
    StartTime=millis();
    
    
    esp_now_add_peer(myData.broadcastAddress, ESP_NOW_ROLE_COMBO, 1, NULL, 0);
    
    esp_now_send(myData.broadcastAddress, (uint8_t *) &myDataSend, sizeof(myDataSend));
    
    
    
 
    esp_now_del_peer(myData.broadcastAddress);
  } 
 

}
 
void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);
  Serial.println("start");
   Serial.print("ESP Board MAC Address:  ");
  Serial.println(WiFi.macAddress());
pinMode(ID1, OUTPUT);
pinMode(ID2, OUTPUT);
pinMode(ID3, OUTPUT);
pinMode(ID4, OUTPUT);
WiFi.begin(ssid, password);
aktBuzzerId=0;
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
strip.begin();
 strip.clear();
 strip.setBrightness(50);
 strip.show();
myData.id=0;
  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  Serial.println('2');
  Serial.println('2');
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
  esp_now_register_recv_cb(OnDataRecv);
  esp_now_register_send_cb(OnDataSent);
}

void loop() {
  delay(1);
  strip.clear();
  strip.fill(color[aktBuzzerId]);
  unsigned long AnswerTime = millis()-StartTime;

  if (aktBuzzerId!=0 && AnswerTime>maxAnswerTime){
    aktBuzzerId=0;
   
        }
if (aktBuzzerId!=0){
  unsigned int OFF = (7*AnswerTime)/maxAnswerTime;

  
  if (OFF!=0){
  strip.fill(color[0],0,OFF);

 
  }  

}
strip.show();
 if (aktBuzzerId==0 ){
    digitalWrite(ID1, LOW);
    digitalWrite(ID2, LOW);
    digitalWrite(ID3, LOW);
    digitalWrite(ID4, LOW);
  
        }
        else if(aktBuzzerId==1){
          digitalWrite(ID1, HIGH);
           Serial.println("ID1");
        }
          else if(aktBuzzerId==2){
          digitalWrite(ID2, HIGH);
           Serial.println("ID2");
        }
          else if(aktBuzzerId==3){
          digitalWrite(ID3, HIGH);
           Serial.println("ID3");
        }
          else if(aktBuzzerId==4){
          digitalWrite(ID4, HIGH);
           Serial.println("ID4");
        }
     
  delay(100);
}
